#!/bin/bash
srcdir="../src"
pdfdir="../pdf"
mdvfile="../mdv-book.pdf"


for f in $srcdir/*.svg; 
do 
echo $f; 
inkscape $f --export-type=pdf --export-filename=$f.pdf
done;

rm $pdfdir/*.pdf
rm $mdvfile
mv $srcdir/*.pdf $pdfdir
echo "Generating $mdvfile"
pdfunite $pdfdir/*.pdf $mdvfile


